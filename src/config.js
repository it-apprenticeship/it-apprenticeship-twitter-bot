module.exports = {
    api: {
        url: 'http://127.0.0.1:9090'
    },
    twitter: {
        consumer_key: '/* API KEY */',
        consumer_secret: '/* API SECRET KEY */',
        access_token: '/* ACCESS TOKEN */',
        access_token_secret: '/* ACCES TOKEN SECRET */'
    }
}
