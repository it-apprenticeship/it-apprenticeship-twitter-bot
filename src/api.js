const axios = require('axios');
const config = require('./config.js')


const api = axios.create({
    baseURL: config.api.url,
    timeout: 1000,
})

/**
 *  Calls the random Note Endpoint from the IT_Apprenticeship_API and returns it
 *  @return data
 */
async function getRandomNote() {
    return await api.get('/notes/random')
        .then((response) => {
            console.log(`Status code: ${response.status}`);
            return response.data
        })
        .catch((error) => {
            console.log(`Error: ${error.message}`)
        })
}

module.exports = getRandomNote
