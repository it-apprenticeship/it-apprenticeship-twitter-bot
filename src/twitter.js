const Twit = require('twit')
const config = require('./config.js')


const twitter = new Twit(config.twitter)

/**
 * Posts a tweet
 * @param msg
 * @param absolutePath
 */
function tweet(msg, absolutePath) {
    absolutePath ? postTweetWithMedia(msg, absolutePath) : postTweet(msg)
}

/**
 * POST Tweet with a message and/or media passed into the function
 * @param msg
 * @param mediaIds
 */
function postTweet(msg, mediaIds) {
    twitter.post(
        'https://api.twitter.com/1.1/statuses/update.json',
        { status: msg, media_ids: mediaIds },
        (error, data, response) => {
            if(error) {
                console.error(error)
            }
        }
    )
}

/**
 *  POST file to twitter media/upload (chunked) API
 * @param msg
 * @param absolutePath
 */
function postTweetWithMedia(msg, absolutePath) {
    twitter.postMediaChunked(
        {file_path: absolutePath},
        (error, data, response) => {
            if(error) {
                console.error(error)
            }
            postTweet(msg, data.media_id_string)
        }
    )
}


module.exports = tweet
